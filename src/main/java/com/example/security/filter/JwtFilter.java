package com.example.security.filter;

import com.example.model.User;
import com.example.security.provider.JwtTokenProvider;
import com.example.service.UserService;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.SignatureException;

import javax.inject.Inject;
import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;

import static com.example.security.provider.JwtsConstans.HEADER_STRING;
import static com.example.security.provider.JwtsConstans.TOKEN_PREFIX;

//@WebFilter(urlPatterns = "/*")
public class JwtFilter implements Filter {

    @Inject
    private JwtTokenProvider jwtTokenProvider;

    @Inject
    private UserService userService;

    @Override
    public void doFilter(ServletRequest request,
                         ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        if (req.getRequestURI().equals("/api/token")) {
            chain.doFilter(request, response);
            return;
        }
        String header = req.getHeader(HEADER_STRING);
        String username = null;
        String authToken;
        HttpServletResponse res = (HttpServletResponse) response;
        if (header == null || !header.startsWith(TOKEN_PREFIX)) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
            return;
        }
        authToken = header.replace(TOKEN_PREFIX,"");
        try {
            username = jwtTokenProvider.getUsernameFromToken(authToken);
        } catch (ExpiredJwtException | SignatureException | IllegalArgumentException e) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }

        if (username == null) {
            ((HttpServletResponse) response).sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
        Optional<User> optionalUser = userService.getByUserName(username);
        if (!optionalUser.isPresent() || !jwtTokenProvider.validateToken(authToken, optionalUser.get())) {
            res.sendError(HttpServletResponse.SC_UNAUTHORIZED);
        }
        System.out.println("Yeah!");
        chain.doFilter(request, response);
    }


}
