package com.example.model;


import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.sql.Timestamp;
import java.time.LocalDateTime;

@Entity
public class TraceEvent {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String status;
    private String type;
    private String category;
    private Timestamp timestamp;
    private Timestamp localTimestamp;
    private String legNetwork;


    public TraceEvent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getLegNetwork() {
        return legNetwork;
    }

    public void setLegNetwork(String legNetwork) {
        this.legNetwork = legNetwork;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Timestamp getLocalTimestamp() {
        return localTimestamp;
    }

    public void setLocalTimestamp(Timestamp localTimestamp) {
        this.localTimestamp = localTimestamp;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "TraceEvent{" +
                "id='" + id + '\'' +
                ", status='" + status + '\'' +
                ", type='" + type + '\'' +
                ", category='" + category + '\'' +
                ", timestamp='" + timestamp + '\'' +
                ", localTimestamp='" + localTimestamp + '\'' +
                ", legNetwork='" + legNetwork + '\'' +
                '}';
    }
}
