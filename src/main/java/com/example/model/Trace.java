package com.example.model;


import javax.persistence.*;

import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.Objects;


@Entity
public class Trace {
    @Id
    @GeneratedValue(generator = "uuid")
    @GenericGenerator(name = "uuid", strategy = "uuid2")
    private String id;
    private String barcode;
    private String type;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinTable(
            name = "trace_trace_event",
            joinColumns = @JoinColumn(name = "trace_id"),
            inverseJoinColumns = @JoinColumn(name = "trace_event_id")
    )
    private List<TraceEvent> events;

    public Trace() {
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<TraceEvent> getEvents() {
        return events;
    }

    public void setEvents(List<TraceEvent> events) {
        this.events = events;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trace trace = (Trace) o;
        return Objects.equals(id, trace.id) &&
                Objects.equals(barcode, trace.barcode) &&
                Objects.equals(type, trace.type) &&
                Objects.equals(events, trace.events);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, barcode, type, events);
    }

    @Override
    public String toString() {
        return "Trace{" +
                "id='" + id + '\'' +
                ", barcode='" + barcode + '\'' +
                ", type='" + type + '\'' +
                ", events=" + events +
                '}';
    }
}

