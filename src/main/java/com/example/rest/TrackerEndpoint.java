package com.example.rest;

import com.example.service.Parser;
import com.example.service.TraceService;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/tracker")
public class TrackerEndpoint {
    @Inject
    private TraceService traceService;

    @Inject
    private Parser parser;

    @GET
    @Produces("text/plain")
    public Response doGet(@QueryParam("keys") List<String> keys) {
        if (keys == null || keys.isEmpty()) {
            return Response.status(400).build();
        }
        traceService.saveOrUpdateAll(parser.parseTraceFromOutServerByKeys(keys));
        return Response.ok(traceService.getAll()).build();
    }
}
