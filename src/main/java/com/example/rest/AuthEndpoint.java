package com.example.rest;

import com.example.dto.AuthTokenDto;
import com.example.dto.AuthUserDto;
import com.example.model.User;
import com.example.security.provider.JwtTokenProvider;
import com.example.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Optional;

@Path("/token")
public class AuthEndpoint {

    @Inject
    private JwtTokenProvider jwtTokenProvider;

    @Inject
    private UserService userService;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getToken(AuthUserDto userDto) {
        if (userDto.getPassword() == null || userDto.getPassword().isEmpty() ||
            userDto.getUsername() == null || userDto.getUsername().isEmpty()) {
            return Response.status(400).build();
        }
        Optional<User> optionalUser = userService.validateAndGetUser(userDto.getUsername(), userDto.getPassword());
        if (!optionalUser.isPresent()) {
            return Response.status(401).build();
        }

        final String token = jwtTokenProvider.generateToken(optionalUser.get());
        return Response
                .ok(new AuthTokenDto(token))
                .build();
    }
}
