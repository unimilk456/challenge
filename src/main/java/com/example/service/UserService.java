package com.example.service;

import com.example.model.Role;
import com.example.model.User;
import com.example.repository.RoleRepository;
import com.example.repository.UserRepository;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.*;

@ApplicationScoped
public class UserService {

    @Inject
    private UserRepository userRepo;

    @Inject
    private RoleRepository roleRepo;

    @PostConstruct
    public void init() {
        userRepo.clear();
        roleRepo.clear();
        roleRepo.add(new Role("ADMIN"));
        roleRepo.add(new Role("USER"));
        User user = new User();
        user.setUserName("admin@mail.ru");
        user.setPassword("qweasd");
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepo.get("ADMIN"));
        roles.add(roleRepo.get("USER"));
        user.setRoles(roles);
        userRepo.addUser(user);
    }
    public Optional<User> validateAndGetUser(String userName, String password) {
        Optional<User> userOptional = userRepo.getUser(userName);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            return password.equals(user.getPassword()) ? userOptional : Optional.empty();
        }
        return Optional.empty();
    }

    public Optional<User> getByUserName(String userName) {
        return userRepo.getUser(userName);
    }
}
