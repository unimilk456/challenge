package com.example.service;

import com.example.model.Trace;
import com.example.model.TraceEvent;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Named
@ApplicationScoped
public class Parser {

    private Timestamp convertStringToTimeStamp(String dateTimeStr) {
        return Timestamp.from(ZonedDateTime.parse("2020-05-08T14:57:18+02:00").toInstant());
    }

    public List<Trace> parseTraceFromOutServerByKeys(List<String> keys) {
        String url = "https://api-gw.dhlparcel.nl/track-trace?key=" +
                String.join("%2C%20", keys); //
        List<Trace> traceList = new ArrayList<>();
        HttpGet httpget = new HttpGet(url);
        try (
                CloseableHttpClient httpclient = HttpClients.createDefault();
                CloseableHttpResponse response = httpclient.execute(httpget)) {
            HttpEntity entity = response.getEntity();
            if (entity == null) {
                return null;
            }
            String responseStr = EntityUtils.toString(entity, "UTF-8");
            if (responseStr == null || responseStr.isEmpty()) {
                return null;
            }
            JSONArray arrTraceJSON = new JSONArray(responseStr);
            for (int i = 0; i < arrTraceJSON.length(); i++) {
                Trace trace = new Trace();
                JSONObject jo = arrTraceJSON.getJSONObject(i);
                trace.setBarcode(jo.getString("barcode"));
                trace.setType(jo.getString("type"));
                JSONArray events = jo.getJSONArray("events");
                List<TraceEvent> traceEventList = new ArrayList<>();
                for (int j = 0; j < events.length(); j++) {
                    TraceEvent traceEvent = new TraceEvent();
                    JSONObject eventJSON = events.getJSONObject(j);
                    JSONObject legJSON;
                    traceEvent.setStatus(eventJSON.getString("status"));
                    if (!eventJSON.has("timestamp")) {
                        break;
                    }
                    traceEvent.setTimestamp(convertStringToTimeStamp(eventJSON.getString("timestamp")));
                    traceEvent.setLocalTimestamp(convertStringToTimeStamp(eventJSON.getString("localTimestamp")));
                    traceEvent.setType(eventJSON.getString("type"));
                    traceEvent.setCategory(eventJSON.getString("category"));
                    legJSON = eventJSON.getJSONObject("leg");
                    traceEvent.setLegNetwork(legJSON.getString("network"));
                    traceEventList.add(traceEvent);
                }
                trace.setEvents(traceEventList);
                traceList.add(trace);
            }

        } catch (
                IOException e) {
            System.err.println("Was unable to extract response body:" + e.getMessage());
        }
        return traceList;
    }
}
