package com.example.service;

import com.example.model.Trace;
import com.example.repository.TraceRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;

@ApplicationScoped
public class TraceService {
    @Inject
    private TraceRepository traceRepository;

    public void save(Trace trace) {
       traceRepository.save(trace);
    }

    public List<Trace> getAll() {
        return traceRepository.getAll();
    }

    public void saveOrUpdateAll(List<Trace> traces) {
        traces.forEach(this::saveOrUpdate);
    }

    public void saveOrUpdate (Trace trace) {
        String id = traceRepository.getIdByBarcode(trace.getBarcode());
        if (id == null) {
            traceRepository.save(trace);
        } else {
            trace.setId(id);
            traceRepository.update(trace);
        }
    }
    public void saveAll(List<Trace> traces) {
        for (int i = 0; i < traces.size(); i++) {
            save(traces.get(i));
        }
    }
}
