package com.example.repository;

import com.example.model.Role;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

@ApplicationScoped
public class RoleRepository {

    @Inject
    private EntityManager em;


    public void clear() {
        em.getTransaction().begin();
        em.createNativeQuery("db.Role.remove( { } )").executeUpdate();
        em.getTransaction().commit();
//        System.out.println(res);
    }

    public void add(Role role) {
        em.getTransaction().begin();
        em.persist(role);
        em.getTransaction().commit();
    }

    public Role get(String name) {
        try {
            return em.
                    createQuery("SELECT r FROM Role r WHERE r.name = :name", Role.class)
                    .setParameter("name", name)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

}
