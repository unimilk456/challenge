package com.example.repository;

import com.example.model.User;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.Optional;

@ApplicationScoped
public class UserRepository {

    @Inject
    private EntityManager em;


    public void clear() {
        em.getTransaction().begin();
        em.createNativeQuery("db.User.remove( { } )").executeUpdate();
        em.getTransaction().commit();
    }

    public void addUser(User user) {
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }

    public Optional<User> getUser(String userName) {
        try {
            return Optional.of(
                    em
                        .createQuery("SELECT user FROM User user WHERE user.userName = :userName", User.class)
                        .setParameter("userName", userName)
                        .getSingleResult()
            );
        } catch (NoResultException ex) {
            return Optional.empty();
        }
     }
}
