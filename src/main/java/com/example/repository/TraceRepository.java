package com.example.repository;

import com.example.model.Role;
import com.example.model.Trace;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import java.util.List;

@ApplicationScoped
public class TraceRepository {
    @Inject
    private EntityManager em;

    public void update(Trace trace) {
        em.getTransaction().begin();
        em.merge(trace);
        em.getTransaction().commit();
    }
    public void save(Trace trace) {
        em.getTransaction().begin();
        em.persist(trace);
        em.getTransaction().commit();
    }

    public String getIdByBarcode(String barcode) {
        try {
            return (String) em.
                    createQuery("SELECT t.id FROM Trace t WHERE t.barcode = :barcode")
                    .setParameter("barcode", barcode)
                    .getSingleResult();
        } catch (NoResultException ex) {
            return null;
        }
    }

    public List<Trace> getAll() {
        return em.createQuery("SELECT t FROM Trace t", Trace.class).getResultList();
    }

    public void saveAll(List<Trace> traces) {
        for (int i = 0; i < traces.size(); i++) {
            save(traces.get(i));
        }
        System.out.println(traces);
    }
}
